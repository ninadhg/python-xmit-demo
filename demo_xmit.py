from xmit import Xmit
from xmit import BitBlink
import os
import subprocess
from subprocess import call
import argparse
import platform
try:
    import SendKeys
except ImportError:
    pass

"""
The application keyboard_leds is available from this url
http://osxbook.com/book/bonus/chapter10/kbdleds/download/keyboard_leds.c
The discussion itself is over here.
http://osxbook.com/book/bonus/chapter10/kbdleds/

"""
FNULL = open(os.devnull, 'w')
win_led = False

class OSXBlink(BitBlink):
    binary = os.path.dirname(os.path.abspath(__file__))+"/keyboard_leds"

    def blink(self,inp):
        if(inp in [True, '1']) :
            call([self.binary, "-c1" ], stdout=FNULL, stderr=subprocess.STDOUT)
        else :
            call([self.binary, "-c0" ], stdout=FNULL, stderr=subprocess.STDOUT)


class RaspberryBlink(BitBlink):
    def __init__(self):
        print "initializing the class"
        self.fdled = open('/sys/class/leds/led0/brightness', 'w')

    def blink(self, inp):
        if (inp in [True, '1']) :
            self.fdled.write( '1\n')
            self.fdled.flush( )
        else :
            self.fdled.write( '0\n')
            self.fdled.flush( )

class WindowsBlink(BitBlink):

    def blink(self, inp):
        global win_led
        if(inp in [True, '1']) :
            #print("Set: %r" % win_led)
            if(not win_led) :
                SendKeys.SendKeys("""{CAPSLOCK}""")
                win_led = True
        else :
            #print("UnSet: %r" % win_led)
            if(win_led) :
                SendKeys.SendKeys("""{CAPSLOCK}""")
                win_led = False


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Testing app for Woluxi")
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-a',
		    dest='asci', help="test ascii data")
    group.add_argument('-c', action="store_true", default=False, 
		    dest='custom', help="test custom data")
    group.add_argument('-d', action="store_true", default=False, 
		    dest='datasheet', help="test datasheet data")
    group.add_argument('-e',
		    dest='errno', help="test errno data")
    group.add_argument('-E',
		    dest='errno_only', help="test errno only  data")
    parser.add_argument('-l', action="store_true", default=False,
		    dest='loop', help="do this in a loop")
    parser.add_argument('-w', action="store_true", default=False,
		    dest='whitelabel', help="test the whitelabel configuration")
    results = parser.parse_args()

    #print str(results)
    #print platform.system()
    if('Darwin' in platform.system()):
        xm = Xmit(OSXBlink())
    elif('Linux' in platform.system()):
	xm = Xmit(RaspberryBlink())
    elif('Windows' in platform.system()):
        print("Please make sure to have caps-lock led as turned off before running")
	xm = Xmit(WindowsBlink())

    xm.hw_id = 0x02
    xm.vendor_id = bytearray(b'\x02\x2B')
    xm.whitelabel = results.whitelabel

    while(True):
        if(results.datasheet):
            xm.mode = 0x10
            byte_data = bytearray(b'\x01\x00\x64')
            xm.xmit_data(bytes(byte_data))

        if(results.asci):
            xm.mode = 0x30
            xm.xmit_data(results.asci)

        if(results.custom):
            xm.mode = 0x20
            byte_data = bytearray(b'\x05\x56\x64\xAE\x34')
            xm.xmit_data(bytes(byte_data))

        if(results.errno):
            xm.mode = 0x00
            byte_data = bytearray(chr(int(results.errno)))
            xm.xmit_data(bytes(byte_data))

        if(results.errno_only):
            xm.mode = 0x00
            xm.errno_only = True
            byte_data = bytearray(chr(int(results.errno_only)))
            xm.xmit_data(bytes(byte_data))

        if(results.loop == False):
            break
