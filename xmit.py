import time
import binascii

_SLEEP_TIME = 0.250

class BitBlink(object):
   def blink(self):
       pass

class Xmit (object):
    last_time_sent = int(round(time.time() * 1000))
    mode = 0x30
    whitelabel = False
    errno_only = False

    def xmit_bytes(self, data):
        """
        """
        self._xmit_preamble()
        length = len(data) - 1
	if (self.errno_only == False) :
           self._xmit_meta_data(length)
           if (((0x30 & self.mode) != 0x20) and self.whitelabel == False) :
               self._xmit_vendor_id()
               self._xmit_hw_id()
        for index in range(0, len(data)):
            print "sending data " + format(ord(data[index]) , '#04x')
            #protocol says transmit the byte twice if equal to preamble.
            if (data[index] == 0xf0):
                self._xmit_data_byte(ord(data[index]))
            self._xmit_data_byte(ord(data[index]))

    def _xmit_bit(self, bit):
        time_sent = int(round(time.time() * 1000))
        #print str(int(round(time.time() * 1000))) + " " + str(bit)
        drift = (time_sent - self.last_time_sent) - 250
        #print("current time " + str(time_sent) + " last " + str(self.last_time_sent) + " diff " + str(drift))
        self.bl.blink(bit)
        time_now = int(round(time.time() * 1000))
        time_diff = (250 - (time_now - self.last_time_sent))/1000.0
        time_diff = ((time_now - time_sent))/1000.0
        #print str(time_diff)
        time.sleep(_SLEEP_TIME - time_diff )
        self.last_time_sent = time_sent

    def _xmit_data_byte(self, data):
        #start bit
        self._xmit_bit(1)
        dt = format(data, 'b').zfill(8)
        for b in dt:
            self._xmit_bit(b)
        #stop bit
        self._xmit_bit(0)
        
    def _xmit_vendor_id(self):
        print "sending vendor id " + format(self.vendor_id[0], '#04x') + " " \
              + format(self.vendor_id[1], '#04x')
        self._xmit_data_byte(self.vendor_id[0])
        self._xmit_data_byte(self.vendor_id[1])

    def _xmit_hw_id(self):
        print "sending hw id " + format(self.hw_id, '#04x')
        self._xmit_data_byte(self.hw_id)

    def _xmit_meta_data(self, len):
        d = self.mode | len
        print "sending meta data " +format(d, '#04x')
        self._xmit_data_byte(d)

    def _xmit_preamble(self):
        d = 0xf0
        print "sending preamble " + format(d, '#04x')
        self._xmit_data_byte(d)
            
    def xmit_chr(self, data):
        print("inside char representation")

    def xmit_int(self, data):
        print("inside int representation")

    def __init__(self,bl):
        self.bl = bl
        self.map = {
            int : self.xmit_int,
            chr: self.xmit_chr,
            bytes: self.xmit_bytes
        }

    def xmit_data(self, data):
        return self.map[type(data)]( data)
